var cfg = {

	// general
	NOTE_NAME: 'et.note',
	NOTE_PERIOD: 30,			// delay between notifications in minutes
	NOTE_TIMEOUT: 10000,		// timeout before clearing notification
	ALARM_NAME: 'et.alarm',

	// notification
	persistent: true,
	type: 'basic',
	title: 'Eye timer notification',
	message: 'Time to make eye excercises!',

	// toolbar button
	toolbar: {
		btn_on: {
			icons: {
				path: {
					'32': 'icons/32_plugin_active.png',
					'16': 'icons/16_plugin_active.png'
				}
			},
			title: {
				title: 'Eye Timer is ON'
			}
		},
		btn_off: {
			icons: {
				path: {
					'32': 'icons/32_plugin_inactive.png',
					'16': 'icons/16_plugin_inactive.png'
				}
			},
			title: {
				title: 'Eye Timer is OFF'
			}
		}
	}

};
