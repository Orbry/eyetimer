const {NOTE_NAME, NOTE_PERIOD, NOTE_TIMEOUT, ALARM_NAME} = cfg;

var plugin_is_on = true;		// general plugin state
var note_timeout_id;			// notification close timeout ID

/*
	set single alarm if persistent note is ON
	otherwise set interval alarm
*/
function setAlarm() {
	if (cfg.persistent) {
		browser.alarms.create(ALARM_NAME, {
			delayInMinutes: NOTE_PERIOD
		});
	} else {
		browser.alarms.create(ALARM_NAME, {
			periodInMinutes: NOTE_PERIOD
		});
	}
}

/*
	disable plugin
*/
function clearAlarm() {
	browser.alarms.clear(ALARM_NAME);
	browser.notifications.clear(NOTE_NAME);
}

function updateAlarm() {
	if (plugin_is_on) {
		setAlarm();
	} else {
		clearAlarm();
	}
}

/*
	update toolbar icon and title
*/
function drawButton(button) {
	browser.browserAction.setIcon(button.icons);
	browser.browserAction.setTitle(button.title);
}

function updateButton() {
	if (plugin_is_on) {
		drawButton(cfg.toolbar.btn_on);
	} else {
		drawButton(cfg.toolbar.btn_off);
	}
}

/*
	create new notification,
	if persistent is ON recreate itself until user interaction
	or plugin disable
*/
function createNote() {
	var note = browser.notifications.create(NOTE_NAME, {
		type: cfg.type,
		title: cfg.title,
		message: cfg.message
	});

	if (cfg.persistent) {
		note.then(() => {
			note_timeout_id = setTimeout(() => {
				browser.notifications.clear(NOTE_NAME).then(() => {
					createNote();
				});
			}, NOTE_TIMEOUT)
		});
	}
}

browser.alarms.onAlarm.addListener(alarm => {
	if (alarm.name === ALARM_NAME) {
		createNote();
	}
});

browser.notifications.onClosed.addListener(id => {
	clearTimeout(note_timeout_id);
	if (cfg.persistent) {
		updateAlarm();
	}
});

browser.browserAction.onClicked.addListener(() => {
	plugin_is_on = !plugin_is_on;
	updateButton();
	updateAlarm();
});

updateAlarm();
